package com.advertisorapp.web.configuration;

import com.advertisorapp.servicelayer.configuration.ServiceConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;

/**
 * Created by Ivan on 15.01.2017.
 *
 * App root spring configuration.
 */
@Configuration
@Import({ ServiceConfiguration.class })
public class AppRootConfig implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Bean
    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {

        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();

        Resource resource = applicationContext.getResource("classpath:config.properties");
        propertyPlaceholderConfigurer.setLocations(resource);

        return propertyPlaceholderConfigurer;
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
