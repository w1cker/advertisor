package com.advertisorapp.web.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.advertisorapp.rest.configuration.RestConfiguration;

/**
 * Created by Ivan on 15.01.2017.
 * <p>
 * Spring Web configuration.
 */
@EnableWebMvc
@Configuration
@Import(RestConfiguration.class)
public class WebConfig extends WebMvcConfigurerAdapter {

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    super.addCorsMappings(registry);
    registry
        .addMapping("/**")
        .allowedMethods("GET", "POST", "DELETE", "PUT")
        .allowCredentials(false);
  }
}
