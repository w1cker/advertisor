package com.advertisorapp.servicelayer.service.impl;

import com.advertisorapp.db.datamodel.Advertisement;
import com.advertisorapp.db.repository.AdvertisementRepository;
import com.advertisorapp.servicelayer.dto.AdvertisementDto;
import com.advertisorapp.servicelayer.mapper.AdvertisementMapper;
import com.advertisorapp.servicelayer.service.AdvertisementService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Ivan on 15.01.2017.
 * <p>
 * Implementation of {@link AdvertisementService}.
 */
@Service
public class AdvertisementServiceImpl implements AdvertisementService {

  @Autowired
  private AdvertisementRepository advertisementRepository;

  public List<AdvertisementDto> getAllAdvertisements() {

    List<AdvertisementDto> advertisements =
            advertisementRepository.findAll()
            .stream()
            .map(AdvertisementMapper::mapAdvertisementDbModelToDto)
            .collect(Collectors.toList());

    return advertisements;
  }

  public AdvertisementDto saveAdvertisement(AdvertisementDto advertisementDto) {

    Advertisement advertisement = AdvertisementMapper.mapAdvertisementDtoToDbModel(advertisementDto);

    Advertisement savedAdvertisement = advertisementRepository.save(advertisement);

    return AdvertisementMapper.mapAdvertisementDbModelToDto(savedAdvertisement);
  }

  public void delete(Long advertisementId) {
    advertisementRepository.delete(advertisementId);
  }

  @Override
  public Optional<AdvertisementDto> findById(Long advertisementId) {
    return null;
  }

  @Override
  public List<Advertisement> findByCity(String city) {
    return advertisementRepository.findByCity(city);
  }

  @Override
  public List<Advertisement> findByCategory(String category) {
    return advertisementRepository.findByCategory(category);
  }

  @Override
  public List<AdvertisementDto> findByCityAndCategory(String city, String category) {

    List<Advertisement> advertisements = new ArrayList<>();


    if(StringUtils.isNotBlank(city) && StringUtils.isNotBlank(category)) {
      advertisements = advertisementRepository.findByCityAndCategory(city, category);
    } else if(StringUtils.isNotBlank(city)) {
      advertisements = advertisementRepository.findByCity(city);
    } else if(StringUtils.isNotBlank(category)){
      advertisements = advertisementRepository.findByCategory(category);
    } else {

      advertisements = advertisementRepository.findAll();
    }

    return advertisements.stream()
            .map(AdvertisementMapper::mapAdvertisementDbModelToDto)
            .collect(Collectors.toList());

  }
}
