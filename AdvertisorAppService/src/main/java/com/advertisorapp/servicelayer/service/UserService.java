package com.advertisorapp.servicelayer.service;

import java.util.List;
import java.util.Optional;

import com.advertisorapp.db.datamodel.User;

/**
 * Created by Ivan on 17.01.2017.
 *
 * User Service.
 */
public interface UserService {

  /**
   * Saves new user.
   * @param userName
   * @param userPassword
   * @param firstName
   * @param lastName
   * @param email
   */
  void saveNewUser(String userName, String userPassword, String firstName, String lastName, String email);

  /**
   * Find user by user name.
   *
   * @param userName the user name.
   * @return user found.
   */
  Optional<User> findByUserName(String userName);

}