package com.advertisorapp.servicelayer.service.impl;

import com.advertisorapp.db.datamodel.User;
import com.advertisorapp.db.repository.UserRepository;
import com.advertisorapp.servicelayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ivan on 17.01.2017.
 *
 * Implementation of {@link UserService}.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @Override
    public void saveNewUser(String userName, String userPassword, String firstName, String lastName, String email) {

        // add user
        User user = new User();
        user.setUserName(userName);
        user.setPassword(userPassword);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
}
