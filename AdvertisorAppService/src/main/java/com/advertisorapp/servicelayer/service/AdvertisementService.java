package com.advertisorapp.servicelayer.service;

import java.util.List;
import java.util.Optional;

import com.advertisorapp.db.datamodel.Advertisement;
import com.advertisorapp.servicelayer.dto.AdvertisementDto;

/**
 * Created by Ivan on 17.01.2017.
 *
 * Advertisement Service.
 */
public interface AdvertisementService {

  List<AdvertisementDto> getAllAdvertisements();

  /**
   * Saves advertisement.
   *
   * @param advertisementDto the advertisement to be saved.
   * @return saved advertisement.
   */
  AdvertisementDto saveAdvertisement(AdvertisementDto advertisementDto);

  /**
   * Deteles advertisement by its id.
   * @param advertisementId the id of the advertisement to be deleted.
   */
  void delete(Long advertisementId);

  Optional<AdvertisementDto> findById(Long advertisementId);

  List<Advertisement> findByCity(String city);

  List<Advertisement> findByCategory(String category);

  List<AdvertisementDto> findByCityAndCategory(String city, String category);
}
