package com.advertisorapp.servicelayer.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.advertisorapp.db.configuration.DataModelConfiguration;
import com.advertisorapp.servicelayer.service.ServiceComponents;

/**
 * Created by Ivan on 15.01.2017.
 *
 *  Spring configuration for the services.
 */
@Configuration
@Import(DataModelConfiguration.class)
@ComponentScan(basePackageClasses = { ServiceComponents.class})
@EnableTransactionManagement
public class ServiceConfiguration {

}
