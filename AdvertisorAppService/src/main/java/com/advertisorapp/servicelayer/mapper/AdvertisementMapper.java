package com.advertisorapp.servicelayer.mapper;

import com.advertisorapp.db.datamodel.Advertisement;

import com.advertisorapp.servicelayer.dto.AdvertisementDto;

/**
 * Created by Ivan on 16.01.2017.
 *
 * Contains methods for mapping advertisements data model to view model and vice versa.
 */
public class AdvertisementMapper {

  /**
   * Maps db model to view model.
   *
   * @param advertisement the advertisement db model to be mapped.
   * @return mapped view model.
   */
  public static AdvertisementDto mapAdvertisementDbModelToDto(Advertisement advertisement) {

    AdvertisementDto dto = new AdvertisementDto();

    dto.setId(advertisement.getId());
    dto.setTitle(advertisement.getTitle());
    dto.setCity(advertisement.getCity());
    dto.setCategory(advertisement.getCategory());
    dto.setShortDescription(advertisement.getShortDescription());
    dto.setLongDescription(advertisement.getLongDescription());
    dto.setPrice(advertisement.getPrice());
    dto.setLikes(advertisement.getLikes());
    dto.setDislikes(advertisement.getDislikes());
    dto.setUsername(advertisement.getUserName());
    return dto;
  }

  /**
   * Maps view model to db model.
   *
   * @param dto the the view model to be mapped.
   * @return mapped db model.
   */
  public static Advertisement mapAdvertisementDtoToDbModel(AdvertisementDto dto) {

    Advertisement advertisement = new Advertisement();

    advertisement.setId(dto.getId());
    advertisement.setTitle(dto.getTitle());
    advertisement.setCity(dto.getCity());
    advertisement.setCategory(dto.getCategory());
    advertisement.setShortDescription(dto.getShortDescription());
    advertisement.setLongDescription(dto.getLongDescription());
    advertisement.setPrice(dto.getPrice());
    advertisement.setUserName(dto.getUsername());
    return advertisement;
  }

}
