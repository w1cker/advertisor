package com.advertisorapp.rest.dto;

import com.advertisorapp.servicelayer.dto.AdvertisementDto;

import java.util.ArrayList;
import java.util.List;

/**
 * View Model for the home page.
 */
public class AdvertisementViewModel {

  private List<AdvertisementDto> advertisements = new ArrayList<>();

  public AdvertisementViewModel(List<AdvertisementDto> advertisements) {
    this.advertisements = advertisements;
  }

  public List<AdvertisementDto> getAdvertisements() {
    return advertisements;
  }

  public void setAdvertisements(List<AdvertisementDto> advertisements) {
    this.advertisements = advertisements;
  }
}
