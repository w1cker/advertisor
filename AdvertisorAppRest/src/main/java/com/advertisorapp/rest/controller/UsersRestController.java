package com.advertisorapp.rest.controller;

import com.advertisorapp.db.datamodel.User;
import com.advertisorapp.rest.dto.UserCredentials;
import com.advertisorapp.rest.dto.UserRegistrationDto;
import com.advertisorapp.servicelayer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Controller for handling CRUD operations on users.
 */
@RestController
public class UsersRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(
            value = "/login-user",
            produces = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.POST)
    public User loginUser(@RequestBody UserCredentials userCredentials) {
        Optional<User> user = userService.findByUserName(userCredentials.getUserName());
        if (user.isPresent()) {
            User match = user.get();
            if (match.getPassword().equals(userCredentials.getPassword())) {
                return match;
            }
            return null;
        }
        return null;
    }

    @RequestMapping(value="/register-user" , method = RequestMethod.POST)
    public void registerUser(@RequestBody UserRegistrationDto userRegistrationDto) {
        if(userRegistrationDto != null) {

            String userName = userRegistrationDto.getUserName();
            String userPassword = userRegistrationDto.getPassword();
            String firstName = userRegistrationDto.getFirstName();
            String lastName = userRegistrationDto.getLastName();
            String email = userRegistrationDto.getEmail();

            userService.saveNewUser(userName, userPassword, firstName, lastName , email);
        }
    }
}
