package com.advertisorapp.rest.controller;

import com.advertisorapp.rest.dto.AdvertisementViewModel;
import com.advertisorapp.servicelayer.dto.AdvertisementDto;
import com.advertisorapp.servicelayer.service.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ivan on 16.01.2017.
 *
 * Rest controller containing CRUD operations for advertisement.
 */
@RestController
public class AdvertisementRestController {

  @Autowired
  private AdvertisementService advertisementService;

  /**
   * Returns advertisement list for the logged in user.
   *
   * @return model containing the list of advertisements.
   */
  @RequestMapping(
      value = "/advertisement-list",
      produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.GET)
  public AdvertisementViewModel getAdvertisementList(
          @RequestParam(required = false, name = "city") String city,
          @RequestParam(required = false, name = "category") String category) {

    List<AdvertisementDto> advertisements = advertisementService.findByCityAndCategory(city, category);

    AdvertisementViewModel restViewDto = new AdvertisementViewModel(advertisements);

    return restViewDto;
  }

  /**
   * Adds new advertisement for the logged in user.
   * @param advertisement the advertisement detail.
   *
   * @return added advertisement detail.
   */
  @RequestMapping(
      value = "/add-advertisement",
      produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.POST)
  public ResponseEntity<AdvertisementDto> addAdvertisement(@RequestBody AdvertisementDto advertisement) {

    AdvertisementDto advertisementAdded = advertisementService.saveAdvertisement(advertisement);

    return new ResponseEntity<AdvertisementDto>(advertisementAdded, HttpStatus.CREATED);
  }


  /**
   *
   * @param advertisement the advertisement detail updated.
   *
   * @return added advertisement.
   */
  @RequestMapping(
      value = "/update-advertisement",
      produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.PUT)
  public ResponseEntity<AdvertisementDto> updateAdvertisement(@RequestBody AdvertisementDto advertisement) {

    AdvertisementDto advertisementUpdated = advertisementService.saveAdvertisement(advertisement);

    return new ResponseEntity<AdvertisementDto>(advertisementUpdated, HttpStatus.OK);
  }

  /**
   * Deletes advertisement for given id.
   *
   * @param advertisementId the advertisement id.
   * @return the deleted advertisement.
   */
  @RequestMapping(
      value = "/delete-advertisement/{advertisementId}",
      produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.DELETE)
  public ResponseEntity<AdvertisementDto> deleteAdvertisement(@PathVariable Long advertisementId) {
      advertisementService.delete(advertisementId);
      return new ResponseEntity<AdvertisementDto>(HttpStatus.NO_CONTENT);
  }
}
