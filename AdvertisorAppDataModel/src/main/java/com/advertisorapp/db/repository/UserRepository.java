package com.advertisorapp.db.repository;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.advertisorapp.db.datamodel.User;

/**
 * Created by Ivan on 15.01.2017.
 *
 * User Repository.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Find user by his name.
     *
     * @param userName the user name.
     * @return found user if present.
     */
    Optional<User> findByUserName(String userName);
}
