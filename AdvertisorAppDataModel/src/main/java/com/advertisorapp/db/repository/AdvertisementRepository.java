package com.advertisorapp.db.repository;

import com.advertisorapp.db.datamodel.Advertisement;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ajovches on 14.04.2017.
 */
public interface AdvertisementRepository extends CrudRepository<Advertisement, Long> {

    /**
     * @return list of all advertisements.
     */
    List<Advertisement> findAll();
    /**
     * Retrieve advertisements filtered by city.
     *
     * @param city the city of the advertisement.
     * @return list of found advertisements.
     */
    List<Advertisement> findByCity(String city);

    List<Advertisement> findByCategory(String category);

    List<Advertisement> findByCityAndCategory(String city, String category);
}
