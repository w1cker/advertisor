package com.advertisorapp.db.configuration;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.advertisorapp.db.repository.RepositoryComponents;

/**
 * Created by Ivan on 15.01.2017.
 *
 * Spring configuration for the data model.
 */
@Configuration
@Import({
    JpaPropertiesProvider.class,
    BasicDataSourceProvider.class,
    EntityManagerFactoryProvider.class,
    JpaTransactionManagerProvider.class
})
@ComponentScan(
    basePackageClasses = {
        RepositoryComponents.class
    })
@EnableJpaRepositories(basePackageClasses = { RepositoryComponents.class })
@EnableTransactionManagement
public class DataModelConfiguration {

//    @Bean
//    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
//
//        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
//
//        Resource resource = new ClassPathResource("config.properties");
//        propertyPlaceholderConfigurer.setLocations(resource);
//
//        return psropertyPlaceholderConfigurer;
//    }

}
