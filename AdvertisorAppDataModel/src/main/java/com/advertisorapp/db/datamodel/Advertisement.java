package com.advertisorapp.db.datamodel;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ADVERTISEMENT")
@Access(AccessType.FIELD)
public class Advertisement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ADVERTISEMENT_ID_SEQUENCE_GENERATOR")
    @SequenceGenerator(
            name = "ADVERTISEMENT_ID_SEQUENCE_GENERATOR",
            sequenceName = "ADVERTISEMENT_ID_SEQUENCE",
            allocationSize = 1,
            initialValue = 10000)
    @Column(name = "ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "TITLE", length = 100, nullable = false)
    private String title;

    @Column(name = "CATEGORY", length = 50, nullable = false)
    private String category;

    @Column(name = "CITY", length = 50, nullable = false)
    private String city;

    @Column(name = "SHORT_DESCRIPTION", length = 150, nullable = false)
    private String shortDescription;

    @Column(name = "LONG_DESCRIPTION", length = 250)
    private String longDescription;

    @Column(name = "PRICE", nullable = false)
    private BigDecimal price;

    @Column(name = "USER_NAME", nullable = false)
    private String userName;

    @Column(name = "LIKES")
    private Integer likes;

    @Column(name = "DISLIKES")
    private Integer dislikes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }
}
