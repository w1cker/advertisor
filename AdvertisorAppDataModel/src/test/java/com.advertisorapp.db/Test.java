package com.advertisorapp.db;

import com.advertisorapp.db.configuration.DataModelConfiguration;
import com.advertisorapp.db.datamodel.Advertisement;
import com.advertisorapp.db.repository.AdvertisementRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

/**
 * Created by ajovches on 14.04.2017.
 */
public class Test {
    public static void main(String[] args) {

        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(DataModelConfiguration.class);

        AdvertisementRepository repo = applicationContext.getBean(AdvertisementRepository.class);

        List<Advertisement> all = repo.findAll();


        System.out.println();

    }
}
