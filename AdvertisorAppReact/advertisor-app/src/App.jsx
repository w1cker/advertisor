import React from 'react';
import thunk from 'redux-thunk';
import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, IndexRedirect, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux';

import PageTemplate from './components/page-template/PageTemplate';
// Pages
import MyAdvertisementsPage from './pages/my-advertisements/MyAdertisementsPage';
import SearchAdvertisementsPage from './pages/search-adverisements/SearchAdvertisementsPage';
import RegisterPage from './pages/register/RegisterPage';
import SignInPage from './pages/sign-in/SignInPage';

import { appRootReducer } from './reducers/appRootReducer';
const rootReducer = combineReducers({
  routing: routerReducer,
  app: appRootReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleWare = applyMiddleware(
  routerMiddleware(browserHistory),
  thunk
);

const store = createStore(
  rootReducer,
  composeEnhancers(middleWare)
);

const history = syncHistoryWithStore(browserHistory, store);

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={PageTemplate}>
          <IndexRedirect to="search-advertisements" />
          <Route path="my-advertisements" component={MyAdvertisementsPage} />
          <Route path="search-advertisements" component={SearchAdvertisementsPage} />
          <Route path="register" component={RegisterPage} />
          <Route path="sign-in" component={SignInPage} />
        </Route>
      </Router>
    </Provider>
  );
}

export default App;
