import { combineReducers } from 'redux';
import { advertisementsReducer } from './advertisements/advertisementsReducer';
import { userReducer } from './user/userReducer';

export const appRootReducer = combineReducers({
  advertisements: advertisementsReducer,
  user: userReducer
});
