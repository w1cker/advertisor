import { handleActions } from 'redux-actions';
import _ from 'lodash';

import { LOG_IN_USER, LOG_OUT_USER } from '../../actions/userActions';

export const userReducer = handleActions({
  [LOG_IN_USER]: loginUser,
  [LOG_OUT_USER]: logoutUser
}, {});

function loginUser(state, action) {
  return action.user || {};
}

function logoutUser(state, action) {
  return {};
}

// Selectors

export function getUser(state) {
  return state.app.user;
}

export function getUsername(state) {
  return getUser(state).userName;
}

export function isUserLoggedIn(state) {
  return !_.isEmpty(state.app.user);
}
