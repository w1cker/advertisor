import _ from 'lodash';
import { getUsername } from '../user/userReducer';

export const getAdvertisements = state => {
  return state.app.advertisements;
};

export const getMyAdvertisements = state => {
  const username = getUsername(state);
  return _.filter(getAdvertisements(state), { username });
};
