import { handleActions } from 'redux-actions';
import _ from 'lodash';

import {
  DELETE_ADVERTISEMENT_SUCCESS,
  FETCH_ADVERTISEMENTS_SUCCESS,
  ADD_ADVERTISEMENTS_SUCCESS
} from '../../actions/advertismentsActions';

export const advertisementsReducer = handleActions({
  [DELETE_ADVERTISEMENT_SUCCESS]: deleteAdvertisement,
  [FETCH_ADVERTISEMENTS_SUCCESS]: setAdvertisements,
  [ADD_ADVERTISEMENTS_SUCCESS]: addAdvertisement
}, []);

function deleteAdvertisement(state, action) {
  return _.filter(state, advertisement => advertisement.id !== action.id);
}

function setAdvertisements(state, action) {
  return action.advertisements;
}

function addAdvertisement(state, action) {
  return [...state, action.a];
}

