import http from '../http/index';

export function loginUser(user) {
  const url = "api/login-user";
  return http.postJson(url, user);
}

export function registerUser(user) {
  const url = "api/register-user";
  return http.postJson(url, user);
}
