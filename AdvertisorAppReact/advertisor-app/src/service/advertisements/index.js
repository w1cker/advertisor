import http from '../http/index';

export function fetchAdvertisements() {
  const url = "api/advertisement-list";
  return http.getJson(url);
}

export function deleteAdvertisement(id) {
  const url = `api/delete-advertisement/${id}`;
  return http.deleteJson(url);
}

export function addAdvertisement(advertisement) {
  const url = 'api/add-advertisement';
  return http.postJson(url, advertisement);
}
