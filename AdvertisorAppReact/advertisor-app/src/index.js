import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.scss';
import './bootstrap/css/bootstrap.css';
import './bootstrap/css/bootstrap-theme.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
