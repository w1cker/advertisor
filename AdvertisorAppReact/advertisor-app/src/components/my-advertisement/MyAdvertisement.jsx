import React from 'react';
import { Panel, Button, Glyphicon, Row, Col } from 'react-bootstrap';
import AdvertisementModal from '../advertisement-modal/AdvertisementModal';
import ConfirmationModal from '../confirmation-modal/ConfirmationModal';

export default class Advertisement extends React.Component {
  constructor() {
    super();
    this.state = {
      showAdvertisementModal: false,
      showConfirmationModal: false
    };
    this.showAdvertisementModal = this.showAdvertisementModal.bind(this);
    this.showConfirmationModal = this.showConfirmationModal.bind(this);
    this.hideAdvertisementModal = this.hideAdvertisementModal.bind(this);
    this.hideConfirmationModal = this.hideConfirmationModal.bind(this);
  }

  showAdvertisementModal() {
    this.setState({ showAdvertisementModal: true});
  }
  showConfirmationModal() {
    this.setState({ showConfirmationModal: true });
  }

  hideAdvertisementModal() {
    this.setState({ showAdvertisementModal: false});
  }
  hideConfirmationModal() {
    this.setState({ showConfirmationModal: false });
  }

  render() {
    const {
      advertisement,
      deleteAdvertisement
    } = this.props;
    const { showAdvertisementModal, showConfirmationModal } = this.state;
    const { shortDescription, title, price } = advertisement;
    return (
      <div className="advertisement modal-container">
        <Row>
          <Col sm={10}>
            <Panel onClick={this.showAdvertisementModal} header={title}>
              <div>{shortDescription}</div>
            </Panel>
          </Col>
          <Col sm={2}>
            <Button
              onClick={this.showConfirmationModal}
              bsSize="large">
              <Glyphicon glyph="remove-sign" />
            </Button>
          </Col>
        </Row>
        <AdvertisementModal
          show={showAdvertisementModal}
          onHide={this.hideAdvertisementModal}
          advertisement={advertisement}
        />
        <ConfirmationModal
          show={showConfirmationModal}
          onConfirm={() => deleteAdvertisement(advertisement.id)}
          onHide={this.hideConfirmationModal}
        />
      </div>
    );
  }
}
