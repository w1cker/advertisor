import React from 'react';
import { Modal, Button } from 'react-bootstrap';

class ConfirmationModal extends React.Component {

  render () {
    const {
      show,
      onHide,
      onConfirm
    } = this.props;

    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          You won't be able to undo this change.<br />
          Are you sure you want to remove this advertisement?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onConfirm}>Yes</Button>
          <Button onClick={onHide}>No</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

ConfirmationModal.propTypes = {
  show: React.PropTypes.bool
};

export default ConfirmationModal;
