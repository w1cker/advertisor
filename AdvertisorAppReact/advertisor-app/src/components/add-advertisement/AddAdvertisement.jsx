import React from 'react';
import { Button } from 'react-bootstrap';
import AddAdvertisementModal from '../add-advertisement-modal/AddAdvertisementModal';

class AddAdvertisement extends React.Component {
  constructor() {
    super();
    this.state = { showModal: false };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { showModal } = this.state;

    return (
      <div className="add-advertisement modal-container">
        <Button onClick={this.showModal} className="add-new-advertisement">Add new advertisement</Button>
        <AddAdvertisementModal show={showModal} onHide={this.hideModal} />
      </div>
    );
  }
}

export default AddAdvertisement;