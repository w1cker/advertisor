import React from 'react';
import { Panel, Glyphicon } from 'react-bootstrap';
import AdvertisementModal from '../advertisement-modal/AdvertisementModal';
import './Advertisement.scss';

export default class Advertisement extends React.Component {
  constructor() {
    super();
    this.state = { showModal: false };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  showModal() {
    this.setState({ showModal: true });
  }

  hideModal() {
    this.setState({ showModal: false });
  }

  render() {
    const { advertisement } = this.props;
    const { showModal } = this.state;
    const {
      shortDescription,
      title,
      price,
      likes,
      dislikes,
    } = advertisement;

    return (
      <div className="advertisement modal-container">
        <Panel onClick={this.showModal}>
          <div className="header">
            <span className="title">
              {title}
            </span>
            <span className="thumbs">
            <span className="thumbs-up">
              <Glyphicon glyph="thumbs-up"/>
              <span className="likes">{likes}</span>
            </span>
            <span className="thumbs-down">
              <Glyphicon glyph="thumbs-down"/>
              <span className="dislikes">{dislikes}</span>
            </span>
            </span>
            <span className="price">
              {price}
            </span>
          </div>
          <br />
          <div>
            {shortDescription}
          </div>
        </Panel>
        <AdvertisementModal
          advertisement={advertisement}
          show={showModal}
          onHide={this.hideModal}
        />
      </div>
    );
  }
}
