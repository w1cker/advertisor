import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button } from 'react-bootstrap';
import * as AdvertisementActions from '../../actions/advertismentsActions';

class AddAdvertisementModal extends React.Component {
  constructor() {
    super();
    this.changeTitle = this.changeTitle.bind(this);
    this.changeShortDescription = this.changeShortDescription.bind(this);
    this.changeLongDescription = this.changeLongDescription.bind(this);
    this.changePrice = this.changePrice.bind(this);
    this.changeContactNumber = this.changeContactNumber.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  changeTitle(event) {
    this.setState({ title: event.target.value });
  }

  changeShortDescription(event) {
    this.setState({ shortDescription: event.target.value });
  }
  changeLongDescription(event) {
    this.setState({ longDescription: event.target.value });
  }

  changePrice(event) {
    this.setState({ price: event.target.value });
  }

  changeContactNumber(event) {
    this.setState({ contactNumber: event.target.value });
  }

  submitForm() {
    this.props.addAdvertisement(this.state);
  }

  render () {
    const { show, onHide } = this.props;
    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Add new advertisement</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <span className="input-label">Title</span> <input onChange={this.changeTitle}/> <br />
          <span className="input-label">Short description</span> <input onChange={this.changeShortDescription} /><br />
          <span className="input-label">Long description</span> <input onChange={this.changeLongDescription} /><br />
          <span className="input-label">Price</span> <input onChange={this.changePrice} /><br />
          <span className="input-label">Contact number</span> <input onChange={this.changeContactNumber} /><br />
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="success" onClick={() => {
            this.submitForm();
            this.props.onHide();
          }}>Add</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

AddAdvertisementModal.propTypes = {
  show: React.PropTypes.bool
};

const mapDispatchToPros = dispatch => bindActionCreators(AdvertisementActions, dispatch);
export default connect(null, mapDispatchToPros)(AddAdvertisementModal);
