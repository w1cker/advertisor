import React from 'react';
import { Modal } from 'react-bootstrap';

class AdvertisementModal extends React.Component {

  render () {
    const { show, onHide, advertisement } = this.props;
    const {
      title,
      longDescription,
      city,
      category
    } = advertisement || {};

    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="description">
            <div><span>Description:</span>{longDescription}</div>
            <div><span>City: </span>{city}</div>
            <div><span>Category: </span>{category}</div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

AdvertisementModal.propTypes = {
  show: React.PropTypes.bool
};

export default AdvertisementModal;