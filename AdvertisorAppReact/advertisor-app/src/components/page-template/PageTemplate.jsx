import React from 'react';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push as pushAction } from 'react-router-redux';
import { Nav, NavItem, PageHeader, Button } from 'react-bootstrap';
import { getUser, isUserLoggedIn } from '../../reducers/user/userReducer';
import { logoutUser as logoutUserAction } from '../../actions/userActions';
import AddAdvertisement from '../../components/add-advertisement/AddAdvertisement';
import './PageTemplate.scss';

class PageTemplate extends React.Component {
  constructor() {
    super();
  }

  render () {
    const { children, isUserLoggedIn, user } = this.props;
    const pathName = this.props.location.pathname;

    return (
      <div className="page-template">
        <PageHeader>
          Advertisor <small>Web app for advertising</small>
          {isUserLoggedIn &&
             <span className="log-out" onClick={() => {
               this.props.logoutUser({});
               this.props.push('/search-advertisements');
             }}>
               (Log out)
             </span>
          }
          {isUserLoggedIn &&
          <span className="user">
            {`${user.firstName} ${user.lastName}`}
          </span>}
        </PageHeader>
        <div className="header">
          <Nav bsStyle="tabs" activeKey={pathName} onSelect={this.props.push}>
            {isUserLoggedIn && <NavItem eventKey="/my-advertisements">My advertisements</NavItem>}
            <NavItem eventKey="/search-advertisements">Search advertisements</NavItem>
            {!isUserLoggedIn && <NavItem eventKey="/sign-in">Sign in</NavItem>}
            {!isUserLoggedIn && <NavItem eventKey="/register">Register</NavItem>}
            {isUserLoggedIn && <AddAdvertisement />}
          </Nav>
        </div>
        {children}
      </div>
    );
  }
}

PageTemplate.propTypes = {
  children: React.PropTypes.node
};

function mapStateToProps(state) {
  return {
    user: getUser(state),
    isUserLoggedIn: isUserLoggedIn(state)
  };
}
const mapDispatchToProps = dispatch => bindActionCreators({
  push: pushAction,
  logoutUser: logoutUserAction
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PageTemplate));
