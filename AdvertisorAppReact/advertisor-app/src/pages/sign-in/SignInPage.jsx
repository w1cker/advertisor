import React from 'react';
import SignInForm from './components/sign-in/SignInForm';

export default function SignInPage() {
  return (
    <div>
      <SignInForm />
    </div>
  );
}
