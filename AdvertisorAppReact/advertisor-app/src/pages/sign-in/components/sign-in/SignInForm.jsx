import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { loginUser as loginUserAction } from '../../../../actions/userActions';
import { push as pushAction } from 'react-router-redux';

import {
  Form,
  FormGroup,
  Col,
  FormControl,
  Checkbox,
  Button,
  ControlLabel
} from 'react-bootstrap';

import './SignInForm.scss';

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.loginUser = this.loginUser.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeUserName = this.onChangeUserName.bind(this);
  }

  loginUser() {
    const { loginUserAction, push } = this.props;
    loginUserAction({
      userName: this.state.userName,
      password: this.state.password
    });
    push('/search-advertisements');
  }

  onChangeUserName(event) {
    this.setState({ userName: event.target.value });
  }

  onChangePassword(event) {
    this.setState({ password: event.target.value });
  }

  render() {
    return (
      <div className="sign-in-form">
        <Form horizontal>
          <FormGroup controlId="formHorizontalEmail">
            <Col componentClass={ControlLabel} sm={2}>
              Username
            </Col>
            <Col sm={10}>
              <FormControl
                onChange={this.onChangeUserName}
                type="email"
                placeholder="Username"
              />
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPassword">
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={10}>
              <FormControl
                onChange={this.onChangePassword}
                type="password"
                placeholder="Password"
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Checkbox>Remember me</Checkbox>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={this.loginUser}>
                Sign in
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  loginUserAction,
  push: pushAction
}, dispatch);

export default connect(null, mapDispatchToProps)(RegisterForm);