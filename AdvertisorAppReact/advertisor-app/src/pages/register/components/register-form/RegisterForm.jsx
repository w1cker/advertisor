import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Form,
  FormGroup,
  Col,
  FormControl,
  Checkbox,
  Button,
  ControlLabel
} from 'react-bootstrap';
import * as UserActions from '../../../../actions/userActions';


class RegisterForm extends React.Component {
  constructor() {
    super();
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  handleFirstNameChange(event) {
    this.setState({firstName: event.target.value});
  }

  handleLastNameChange(event) {
    this.setState({lastName: event.target.value});
  }

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  handleUserNameChange(event) {
    this.setState({userName: event.target.value});
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  submitForm() {
    console.log(this.state);
    this.props.registerUser(this.state);
  }

  render() {
    return (
      <div className="sign-in-form">
        <Form horizontal>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Username
            </Col>
            <Col sm={8}>
              <FormControl onChange={this.handleUserNameChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Name
            </Col>
            <Col sm={8}>
              <FormControl onChange={this.handleFirstNameChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Surname
            </Col>
            <Col sm={8}>
              <FormControl onChange={this.handleLastNameChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Email
            </Col>
            <Col sm={8}>
              <FormControl onChange={this.handleEmailChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Password
            </Col>
            <Col sm={8}>
              <FormControl onChange={this.handlePasswordChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={4} sm={8}>
              <Button onClick={this.submitForm}>
                Register
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) { return bindActionCreators(UserActions, dispatch); }

export default connect(null, mapDispatchToProps)(RegisterForm);
