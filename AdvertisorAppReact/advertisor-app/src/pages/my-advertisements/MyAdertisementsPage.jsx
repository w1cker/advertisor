import React from 'react';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { connect } from 'react-redux';
import MyAdvertisement from '../../components/my-advertisement/MyAdvertisement';
import { getMyAdvertisements } from '../../reducers/advertisements/advertisementsSelectors';
import * as AdvertisementsActions from '../../actions/advertismentsActions';

class MyAdvertisementsPage extends React.Component {

  componentDidMount() {
    this.props.fetchAdvertisements();
  }

  render () {
    const { advertisements, deleteAdvertisement } = this.props;
    return (
      <div>
        {_.map(advertisements, advertisement =>
          <MyAdvertisement
            key={advertisement.id}
            advertisement={advertisement}
            deleteAdvertisement={deleteAdvertisement}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { advertisements: getMyAdvertisements(state)};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(AdvertisementsActions , dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyAdvertisementsPage);
