import React from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { fetchAdvertisements as fetchAdvertisementsAction }
  from '../../actions/advertismentsActions';

// Components
import Advertisement from '../../components/advertisement/Advertisement';

// Selectors
import { getAdvertisements } from '../../reducers/advertisements/advertisementsSelectors';

class SearchAdvertisementsPage extends React.Component {
  constructor() {
    super();
    this.state = { mostLikedFirst: false };
    this.updateCheckBoxState = this.updateCheckBoxState.bind(this);
    this.updateFromPrice = this.updateFromPrice.bind(this);
    this.updateToPrice = this.updateToPrice.bind(this);
  }

  componentDidMount() {
    const { fetchAdvertisements } = this.props;
    fetchAdvertisements();
  }

  updateCheckBoxState(event) {
    const value = event.target.checked;
    this.setState({ mostLikedFirst: value });
  }

  updateFromPrice(event) {
    this.setState({ priceFrom: event.target.value });
  }

  updateToPrice(event) {
    this.setState({ priceTo: event.target.value });
  }

  render() {
    const { advertisements } = this.props;
    const { mostLikedFirst } = this.state;

    const filteredByLikes = mostLikedFirst ? _.sortBy(advertisements, a => -a.likes) : advertisements;
    const filteredByPrice = (this.state.priceFrom && this.state.priceTo) ?
      _.filter(filteredByLikes, a => a.price >= this.state.priceFrom && a.price <= this.state.priceTo)
      : filteredByLikes;

    return (
      <div className="advertisements-search-results">
        <Panel bsStyle="primary" header="Filters">
          <span>Most liked first: </span>
          <input checked={mostLikedFirst} onChange={this.updateCheckBoxState} type="checkbox" />
          <br />
          <span>Price
            <input placeholder="From" onChange={this.updateFromPrice} />
            <input placeholder="To" onChange={this.updateToPrice} />
          </span>
        </Panel>
        {_.map(filteredByPrice, advertisement =>
          <Advertisement
            key={advertisement.id}
            advertisement={advertisement}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { advertisements: getAdvertisements(state) }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchAdvertisements: fetchAdvertisementsAction
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchAdvertisementsPage);
