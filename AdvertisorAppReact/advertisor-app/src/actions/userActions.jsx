export const LOG_IN_USER = 'LOG_IN_USER';
export const LOG_OUT_USER = 'LOG_OUT_USER';

import * as UsersService from '../service/users/index';

function loginUserSuccess(user) {
  return {
    type: LOG_IN_USER,
    user
  };
}

export function loginUser(user) {
  return dispatch => UsersService.loginUser(user).then(response => dispatch(loginUserSuccess(response)));
}

export function registerUser(user) {
  return dispatch => UsersService.registerUser(user).then(() => console.log('Registration succeeded')).catch(() => console.log('Registration failed'));
}

export function logoutUser(user) {
  return {
    type: LOG_OUT_USER,
    user
  };
}

