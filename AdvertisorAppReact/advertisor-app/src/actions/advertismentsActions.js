export const DELETE_ADVERTISEMENT_SUCCESS = 'DELETE_ADVERTISEMENT_SUCCESS';
export const FETCH_ADVERTISEMENTS_SUCCESS = 'FETCH_ADVERTISEMENTS_SUCCESS';
export const ADD_ADVERTISEMENTS_SUCCESS = 'ADD_ADVERTISEMENTS_SUCCESS';

import * as AdvertisementsService from '../service/advertisements/index';

function fetchAdvertisementsSuccess(response) {
  return {
    type: FETCH_ADVERTISEMENTS_SUCCESS,
    advertisements: response.advertisements
  };
}

export function fetchAdvertisements() {
  return dispatch => {
    AdvertisementsService.fetchAdvertisements().then(response => {
      dispatch(fetchAdvertisementsSuccess(response));
    });
  }
}

function addAdvertisementSuccess(a) {
  return {
    type: ADD_ADVERTISEMENTS_SUCCESS,
    a
  }
}
export function addAdvertisement(a) {
  return (dispatch, getState) => {
    const username = getState().app.user.userName;
    const req = Object.assign({
      id: 0,
      title: '',
      city: '',
      category: '',
      shortDescription: '',
      longDescription: '',
      price: 0,
      likes: 0,
      dislikes: 0,
      username
    }, a);
    AdvertisementsService.addAdvertisement(req).then(res => dispatch(addAdvertisementSuccess(res)));
  }
}
export function deleteAdvertisement(id) {
  return dispatch => {
    AdvertisementsService.deleteAdvertisement(id).then(() => dispatch(deleteAdvertisementSuccess(id)));
  }
}

export function deleteAdvertisementSuccess(id) {
  return {
    type: DELETE_ADVERTISEMENT_SUCCESS,
    id
  };
}
